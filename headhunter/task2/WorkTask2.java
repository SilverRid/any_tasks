package head;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WorkTask2 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int m = in.nextInt();
        int n = in.nextInt();

        int[][] arr = new int[n][m];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = in.nextInt();
            }
        }
        //Получаем список регионов
        List<Square> listSqures = new ArrayList<>();
        Square square;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] == 1) {
                    square = new Square(i, j, i, j);

                    calculateSquare(arr, i, j, square);

                    if (square.counter > 1) {
                        listSqures.add(square);
                    }
                }
            }
        }
        //Проверка количества плодородных участков в регионе
        checkCounterPlate(arr, listSqures);

        Collections.sort(listSqures);

        if (listSqures.isEmpty()) {
            System.out.println(0);
        } else {
            System.out.println(listSqures.get(listSqures.size() - 1).square());
        }
    }

    private static void checkCounterPlate(final int[][] arr, final List<Square> squareList) {
        for (Square square : squareList) {
            checkCounterPlateHelper(arr, square);
        }
    }

    private static void checkCounterPlateHelper(final int[][] arr, final Square square) {
        int counter = 0;
        for (int i = square.startI; i <= square.endI; i++) {
            for (int j = square.startJ; j <= square.endJ; j++) {
                if (arr[i][j] != 0) {
                    counter++;
                }
            }
        }
        square.counter = counter;
    }

    /**
     * Определяем регион
     */
    private static void calculateSquare(final int[][] arr, final int i, final int j, final Square square) {
        coordinatesSquare(i, j, square);
        square.counter++;
        arr[i][j] = 8;

        if (((i - 1) >= 0 && (j - 1) >= 0)) {
            if (arr[i - 1][j - 1] == 1) {
                calculateSquare(arr, i - 1, j - 1, square);
            }
        }
        if ((j - 1) >= 0) {
            if (arr[i][j - 1] == 1) {
                calculateSquare(arr, i, j - 1, square);
            }
        }
        if ((i - 1) >= 0) {
            if (arr[i - 1][j] == 1) {
                calculateSquare(arr, i - 1, j, square);
            }
        }
        if (((i + 1) < arr.length && (j + 1) < arr[i].length)) {
            if (arr[i + 1][j + 1] == 1) {
                calculateSquare(arr, i + 1, j + 1, square);
            }
        }
        if ((j + 1) < arr[i].length) {
            if (arr[i][j + 1] == 1) {
                calculateSquare(arr, i, j + 1, square);
            }
        }
        if ((i + 1) < arr.length) {
            if (arr[i + 1][j] == 1) {
                calculateSquare(arr, i + 1, j, square);
            }
        }

        if (((i + 1) < arr.length && (j - 1) >= 0)) {
            if (arr[i + 1][j - 1] == 1) {
                calculateSquare(arr, i + 1, j - 1, square);
            }
        }

        if (((i - 1) >= 0 && (j + 1) < arr[i].length)) {
            if (arr[i - 1][j + 1] == 1) {
                calculateSquare(arr, i - 1, j + 1, square);
            }
        }
    }

    /**
     * Проверяем координаты региона
     */
    private static void coordinatesSquare(final int i, final int j, final Square square) {
        if (i < square.startI) {
            square.startI = i;
        }
        if (j < square.startJ) {
            square.startJ = j;
        }

        if (i > square.endI) {
            square.endI = i;
        }
        if (j > square.endJ) {
            square.endJ = j;
        }
    }

    public static class Square implements Comparable<Square> {
        // Начальные координаты региона
        int startI;

        int startJ;

        // Конечные координаты региона
        int endI;

        int endJ;

        // Количество плодородных участков
        int counter;

        public Square() {
        }

        public Square(final int startI, final int startJ, final int endI, final int endJ) {
            this.startI = startI;
            this.endI = endI;
            this.startJ = startJ;
            this.endJ = endJ;
        }

        public int square() {
            return ((endI - startI + 1) * (endJ - startJ + 1));
        }

        public double efficiencyToBuy() {
            double efficiency = (double) counter / square();
            return Math.floor(efficiency * 100) / 100;
        }

        @Override
        public int compareTo(final Square o) {
            if (Double.compare(efficiencyToBuy(), o.efficiencyToBuy()) == 0) {
                return square() - o.square();
            } else {
                return Double.compare(efficiencyToBuy(), o.efficiencyToBuy());
            }
        }

        @Override
        public String toString() {
            return "Node{" +
                "startI=" + startI +
                ", startJ=" + startJ +
                ", endI=" + endI +
                ", endJ=" + endJ +
                '}';
        }
    }
}