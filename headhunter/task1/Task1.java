package head;

import java.util.Scanner;

/**
 * Пример 1
 * Ввод:
 * <p>
 * 3 4 11
 * 1 1
 * 2 2
 * 3 3
 * - 4
 * <p>
 * Вывод:
 * <p>
 * 5
 * <p>
 * Пример 2
 * Ввод:
 * <p>
 * 5 5 10
 * 5 1
 * 1 3
 * 1 3
 * 1 3
 * 1 3
 * <p>
 * Вывод:
 * <p>
 * 6
 * <p>
 * Пример 3
 * Ввод:
 * <p>
 * 6 4 10
 * 4 2
 * 2 1
 * 4 8
 * 6 5
 * 1 -
 * 7 -
 * <p>
 * Вывод:
 * <p>
 * 4
 *
 * @author Sergei Iurochkin
 * @link https://github.com/SilverRid
 */

public class Task1 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int s = in.nextInt();

        int[] arrN = new int[n];
        int[] arrM = new int[m];

        in.nextLine();
        int n1 = 0, m1 = 0;
        while (n1 < n || m1 < m) {
            String[] c = in.nextLine().split(" ");
            if (n1 < n) {
                arrN[n1++] = Integer.parseInt(c[0]);
            }
            if (m1 < m) {
                arrM[m1++] = Integer.parseInt(c[1]);
            }

        }

        int countMax1 = getCountMax(arrN, arrM, s);
        int countMax2 = getCountMax(arrM, arrN, s);
        System.out.println(Math.max(countMax1, countMax2));
    }

    private static int getCountMax(final int[] arrN, final int[] arrM, final int s) {
        int totalSum;
        int totalCount;
        int countMax = 0;
        int sumFirstColumn = 0;
        int countResumeFirstColumn = 0;

        for (int i = 0; i <= arrN.length; i++) {
            totalSum = sumFirstColumn;
            totalCount = countResumeFirstColumn;

            for (int j = 0; j < arrM.length; j++) {
                totalSum += arrM[j];
                if (totalSum <= s) {
                    totalCount++;
                    if (countMax < totalCount) {
                        countMax = totalCount;
                    }
                } else {
                    break;
                }
            }
            if (i == arrN.length) {
                break;
            }

            sumFirstColumn += arrN[i];
            if (sumFirstColumn > s) {
                break;
            }
            countResumeFirstColumn++;

        }
        return countMax;
    }
}
